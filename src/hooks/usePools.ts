import { POOLS_INFO } from '../config/constants/Pools'

export const usePools = () => {
  return POOLS_INFO
}
