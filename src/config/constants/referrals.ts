export const REFERRER_QUERY_PARAM = 'r'
export const REFERRER_STORAGE_NAME = 'referrer'

export const REFERRALS_ACTIONS: Record<number, string> = {
  1: 'Presale purchase',
  2: 'Staking reward',
}
