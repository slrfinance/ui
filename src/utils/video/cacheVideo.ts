const fetchPromises = new Map<string, Promise<Response>>()

export const prefetchVideo = async (videoFileUrl: string) => {
  if (fetchPromises.has(videoFileUrl)) {
    return fetchPromises.get(videoFileUrl) as Promise<Response>
  } else {
    const promise = fetchVideoAndCache(videoFileUrl)

    fetchPromises.set(videoFileUrl, promise)

    const response = await promise

    fetchPromises.delete(videoFileUrl)

    return response
  }
}

const fetchVideoAndCache = async (videoFileUrl: string) => {
  const cache = await window.caches.open('slr-video-pre-cache')
  // Сначала проверяем, есть ли видео в кэше.
  const cacheResponse = await cache.match(videoFileUrl)

  // Вернем кэшированный ответ, если видео в кэше.
  if (cacheResponse) {
    return cacheResponse
  }

  // В противном случае получаем видео из сети.
  const networkResponse = await fetch(videoFileUrl)

  // Добавляем ответ в кэш и параллельно возвращаем ответ сети.
  await cache.put(videoFileUrl, networkResponse.clone())

  return networkResponse
}

export const playVideoFromCache = async (video: HTMLVideoElement, videoFileUrl: string) => {
  video.load()

  const response = await prefetchVideo(videoFileUrl)
  const data = await response.clone().blob()

  video.src = URL.createObjectURL(data)
}
